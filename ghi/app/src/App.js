import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />

      {/* Define the application's routes using the Routes component */}
      <Routes>
        {/* Define the default route (index) to render the MainPage component */}
        <Route index element={<MainPage />} />

        {/* Define the route for "locations". It's a parent route for location-related components */}
        <Route path="locations">
          {/* Nested route under "locations" for creating a new location, renders the LocationForm component */}
          <Route path="new" element={<LocationForm />} />
        </Route>

        {/* Define the route for "conferences". It's a parent route for conference-related components */}
        <Route path="conferences">
          {/* Nested route under "conferences" for creating a new conference, renders the ConferenceForm component */}
          <Route path="new" element={<ConferenceForm />} />
        </Route>

        {/* Define the route for "attendees". It's a parent route for attendee-related components */}
        <Route path="attendees">
          {/* Default route under "attendees", renders the AttendeesList component */}
          <Route index element={<AttendeesList />} />
          {/* Nested route under "attendees" for creating a new attendee, renders the AttendConferenceForm component */}
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>

        {/* Define the route for "presentations". It's a parent route for presentation-related components */}
        <Route path="presentations">
          {/* Nested route under "presentations" for creating a new presentation, renders the PresentationForm component */}
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;

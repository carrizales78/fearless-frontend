import React, { useEffect, useState } from 'react';

function LocationForm() {
  // State for storing the list of states fetched from the API
  const [states, setStates] = useState([]);

  // State for managing form data as a single object
  const [formData, setFormData] = useState({
    name: '',
    room_count: '',
    city: '',
    state: ''
  });

  // Function to fetch states data from the API
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setStates(data.states);
      } 
    } 

  // useEffect hook to fetch states data when the component mounts
  useEffect(() => {
    fetchData();
  }, []);

  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        // Reset form data after successful submission
        setFormData({
          name: '',
          room_count: '',
          city: '',
          state: ''
        });
      }
    }  

  // Function to handle form field changes
  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevFormData => ({
      ...prevFormData,
      [name]: value
    }));
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            {/* Name input */}
            <div className="form-floating mb-3">
              <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            {/* Room count input */}
            <div className="form-floating mb-3">
              <input value={formData.room_count} onChange={handleFormChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
              <label htmlFor="room_count">Room count</label>
            </div>

            {/* City input */}
            <div className="form-floating mb-3">
              <input value={formData.city} onChange={handleFormChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
              <label htmlFor="city">City</label>
            </div>

            {/* State selection */}
            <div className="mb-3">
              <select value={formData.state} onChange={handleFormChange} required name="state" id="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(state => (
                  <option key={state.abbreviation} value={state.abbreviation}>
                    {state.name}
                  </option>
                ))}
              </select>
            </div>

            {/* Submit button */}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;

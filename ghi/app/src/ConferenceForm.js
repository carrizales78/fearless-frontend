import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  // State for storing the list of locations fetched from the API
  const [locations, setLocations] = useState([]);

  // State for managing all form fields in a single object
  const [formData, setFormData] = useState({
    name: '',
    starts: '',
    ends: '',
    description: '',
    max_presentations: '',
    max_attendees: '',
    location: '',
  });

  // Function to fetch locations data from the API
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      } else {
        // Handle HTTP errors here
      }
    } catch (error) {
      // Handle network errors here
    }
  }

  // useEffect hook to fetch locations data when the component mounts
  useEffect(() => {
    fetchData();
  }, []);

  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const conferenceUrl = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        // Clear form data after successful submission
        setFormData({
          name: '',
          starts: '',
          ends: '',
          description: '',
          max_presentations: '',
          max_attendees: '',
          location: '',
        });

      } else {
        // Handle HTTP errors here
      }
    } catch (error) {
      // Handle network errors here
    }
  }

  // Function to handle changes in form fields
  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevFormData => ({
      ...prevFormData,
      [name]: value
    }));
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            
            {/* Input for conference name */}
            <div className="form-floating mb-3">
              <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            {/* Input for conference start date */}
            <div className="form-floating mb-3">
              <input value={formData.starts} onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>

            {/* Input for conference end date */}
            <div className="form-floating mb-3">
              <input value={formData.ends} onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>

            {/* Textarea for conference description */}
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea value={formData.description} onChange={handleFormChange} id="description" rows="3" name="description" className="form-control"></textarea>
            </div>

            {/* Input for maximum presentations */}
            <div className="form-floating mb-3">
              <input value={formData.max_presentations} onChange={handleFormChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>

            {/* Input for maximum attendees */}
            <div className="form-floating mb-3">
              <input value={formData.max_attendees} onChange={handleFormChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>

            {/* Dropdown for selecting location */}
            <div className="mb-3">
              <select value={formData.location} onChange={handleFormChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => (
                  <option key={location.id} value={location.id}>{location.name}</option>
                ))}
              </select>
            </div>

            {/* Submit button */}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ConferenceForm;

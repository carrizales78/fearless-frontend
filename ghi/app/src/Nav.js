import { NavLink } from "react-router-dom";

function Nav() {
  return (
    
    // Main header containing the navigation bar
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        {/* Container to ensure the content aligns well with other elements */}
        <div className="container-fluid">
          {/* Brand logo or name, linking to the home page */}
          <NavLink className="navbar-brand" to="/">
            Conference GO!
          </NavLink>
          
          {/* Toggler for mobile view, toggles the navigation links */}
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          {/* Navigation links that collapse in mobile view */}
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {/* List of navigation links */}
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {/* Individual navigation item - Home */}
              <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">
                  Home
                </NavLink>
              </li>

              {/* Navigation item - Link to create a new location */}
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/locations/new">
                  New location
                </NavLink>
              </li>

              {/* Navigation item - Link to create a new conference */}
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/conferences/new">
                  New conference
                </NavLink>
              </li>

              {/* Navigation item - Link to create a new presentation */}
              <li className="nav-item">
                <NavLink className="nav-link" aria-current="page" to="/presentations/new">
                  New presentation
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}

export default Nav;

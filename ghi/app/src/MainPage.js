import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

// ConferenceColumn is a functional component that renders a list of conferences
function ConferenceColumn(props) {
  return (

    <div className="col">

      {/* Mapping each conference data to a card */}
      {props.list.map(data => {
        const conference = data.conference;
        return (
          <div key={conference.href} className="card mb-3 shadow">

            {/* Conference image */}
            <img src={conference.location.picture_url} className="card-img-top" alt={conference.name} />
            <div className="card-body">

              {/* Conference title */}
              <h5 className="card-title">{conference.name}</h5>

              {/* Conference location */}
              <h6 className="card-subtitle mb-2 text-muted">
                {conference.location.name}
              </h6>

              {/* Conference description */}
              <p className="card-text">
                {conference.description}
              </p>
            </div>

            {/* Conference date range */}
            <div className="card-footer">
              {new Date(conference.starts).toLocaleDateString()} - {new Date(conference.ends).toLocaleDateString()}
            </div>
          </div>
        );
      })}
    </div>
  );
}

// MainPage is a functional component that fetches and displays conference data
const MainPage = (props) =>  {

  // State for storing conference data in columns
  const [conferenceColumns, setConferenceColumns] = useState([[], [], []]);

  // Function to fetch conference data
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);
        const columns = [[], [], []];
        let i = 0;
        for (const conferenceResponse of responses) {
          if (conferenceResponse.ok) {
            const details = await conferenceResponse.json();
            columns[i].push(details);
            i = (i + 1) % 3; // Cycle through the columns
          } else {
            console.error(conferenceResponse);
          }
        }
        setConferenceColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  // Fetch data when the component mounts
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {/* Hero section with welcome message and link to attend a conference */}
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">Conference GO!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The only resource you'll ever need to plan and run your in-person or virtual conference for thousands of attendees and presenters.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/attendees/new" className="btn btn-primary btn-lg px-4 gap-3">Attend a conference</Link>
          </div>
        </div>
      </div>
      
      {/* Display upcoming conferences in columns */}
      <div className="container">
        <h2>Upcoming conferences</h2>
        
        <div className="row">
          {/* Mapping each column of conference data to a ConferenceColumn */}
          {conferenceColumns.map((conferenceList, index) => (
            <ConferenceColumn key={index} list={conferenceList} />
          ))}
        </div>
      </div>
    </>
  );
}

export default MainPage;

import { useEffect, useState } from "react";

function AttendeesList() {
  const [attendees, setAttendees] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8001/api/attendees/');
    
    if (response.ok) {
      const data = await response.json();
      setAttendees(data.attendees)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    // Render a table to display attendees
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {/* Map through each attendee passed in props and display them in table rows */}
        {attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              {/* Display the attendee's name */}
              <td>{attendee.name}</td>
              {/* Display the conference associated with the attendee */}
              <td>{attendee.conference}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AttendeesList;

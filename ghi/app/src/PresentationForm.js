import React, { useState, useEffect } from 'react';

function PresentationForm() {
  // State for storing the list of conferences
  const [conferences, setConferences] = useState([]);

  // State for managing form data
  const [formData, setFormData] = useState({
    presenter_name: '',
    presenter_email: '',
    company_name: '',
    title: '',
    synopsis: '',
    conference: '',
  });

  // Function to fetch conference data from the API
  const getData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  // useEffect hook to fetch data when the component mounts
  useEffect(() => {
    getData();
  }, []);

  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const { conference } = formData;
    const url = `http://localhost:8000/api/conferences/${conference}/presentations/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      // Clear form data after successful submission
      setFormData({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: '',
      });
    }
  };

  // Function to handle changes in form fields
  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;

    setFormData({
      // Previous form data is spread into our new state object
      ...formData,

      // Add the currently engaged input key and value
      [inputName]: value 
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">

            {/* Input fields for presentation form */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.presenter_name} placeholder="Presenter name" required name="presenter_name" className="form-control" />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.presenter_email} placeholder="Presenter email" required name="presenter_email" type="email" id="presenter_email" className="form-control" />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>

            {/* Dropdown for selecting conference */}
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.conference} required name="conference" className="form-select" id="conference">
                <option value="">Choose a conference</option>
                {conferences.map(conference => (
                  <option key={conference.id} value={conference.id}>{conference.name}</option>
                ))}
              </select>
            </div>
            
            {/* Submit button */}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;

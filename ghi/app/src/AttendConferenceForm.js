import React, { useEffect, useState } from 'react';

function AttendConferenceForm() {
  // State for storing the list of conferences
  const [conferences, setConferences] = useState([]);

  // State for managing form data
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    conference: ''
  });

  // State to track if the user has successfully signed up
  const [hasSignedUp, setHasSignedUp] = useState(false);

  // Function to fetch conference data from the API
  const getData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  // useEffect hook to fetch data when the component mounts
  useEffect(() => {
    getData();
  }, []);

  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8001${formData.conference}attendees/`;
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      // Clear form data and indicate successful sign up
      setFormData({
        name: '',
        email: '',
        conference: ''
      });

      setHasSignedUp(true);
    }
  };

  // Function to handle input changes in the form
  const handleChangeName = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  // Class name for the form, hides the form on successful sign up
  const formClasses = (!hasSignedUp) ? '' : 'd-none';

  // Class name for the success message, shows message on successful sign up
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">

         {/* Logo for the form */}
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
        </div>

        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              
              {/* Form for conference registration */}
              <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">Please choose which conference you'd like to attend.</p>

                {/* Dropdown to select a conference */}
                <div className="mb-3">
                  <select onChange={handleChangeName} name="conference" id="conference" required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => (
                      <option key={conference.href} value={conference.href}>{conference.name}</option>
                    ))}
                  </select>
                </div>

                <p className="mb-3">Now, tell us about yourself.</p>

                {/* Input fields for name and email */}
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>

                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>

                {/* Submit button */}
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>

              {/* Success message displayed after successful sign up */}
              <div className={messageClasses} id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendConferenceForm;
